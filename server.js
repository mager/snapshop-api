var express = require('express')
var app = express();
var request = require('request');
var querystring = require('querystring');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


app.get('/', function (req, res) {
    // Get the query param
    var searchTerm = req.query.name;
    var lat = req.query.lat;
    var lng = req.query.lng;

    var options = {
        method: 'GET',
        url: 'https://api.goodzer.com/products/v0.1/search_stores/',
        qs: {
            query: searchTerm,
            lat: lat,
            lng: lng,
            radius: '5',
            priceRange: '30:120',
            apiKey: ' 14f5e533a1fe37b55e049d4fae1df462'
        },
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        var parsed = JSON.parse(body);
        for (i=0; i<parsed.stores.length; i++) {
            if (parsed.stores[i].locations && parsed.stores[i].locations.length) {
                var location = parsed.stores[i].locations[0];
                var address = location.address + ', ' + location.city + ', ' + location.state + ' ' + location.zipcode;
                var response = {
                    'address': address
                };
                res.json(response);
                break;
            }
        }
    });
})

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
})
